# Instalación Herramientas en Windows

*Probado en Windows 10 Pro 64bits Build 19041 y 18363

## Docker Desktop for Windows

Docker utiliza virtualización en el Sistema Operativo Anfitrión o _Host_ para correr software en contenedores. En Windows esta virtualización puede ser realizada con distintos _backends_:

- WSL 2
- Hyper-V
- Virtual Machine (legacy)

Para esta guía nos vamos a focalizar en la utilización de WSL 2 por su simpleza y el hecho que es el método más optimizado y reciente. Las otras alternativas también son teoricamente posibles, pero queda a cargo del lector investigar un poco más sobre ellas.

#### WSL 2

[Windows Subsystem for Linux](https://docs.microsoft.com/en-us/windows/wsl/about) es una capa de compatibilidad que permite correr archivos binarios ejecutables de Linux nativamente en Windows 10. WSL 2 fue lanzado en Junio 2019 e introdujo un kernel de Linux real a traves de la utilización de un subset de funcionalidades optimizadas de Hyper-V. Docker funciona bastante bien y de manera muy simple cuando utilizamos este backend.

Los requerimientos del sistema son los siguientes:

- For x64 systems: Version 1903 or higher, with Build 18362 or higher.
- For ARM64 systems: Version 2004 or higher, with Build 19041 or higher.
- Builds lower than 18362 do not support WSL 2. Use the Windows Update Assistant to update your version of Windows.

Cabe destacar que este backend ***solo es compatible con computadoras con versiones de Windows de 64bits***. ARM64 también está soportado, aunque no se ha probado la compatibilidad del resto de las herramientas en dicha plataforma.

De cumplir con los mismos proceder con la instalación. Para instrucciones detalladas referirse a [la guía oficial en la documentación de Microsoft](https://docs.microsoft.com/en-us/windows/wsl/install-win10):

- Abrir una instancia de Power Shell como Administrador
 - Correr el siguiente comando para habilitar WSL:
 ```bash
 dism.exe /online /enable-feature /featurename:Microsoft-Windows-Subsystem-Linux /all /norestart
 ```
  -  Correr el siguiente comando para habilitar la funcionalidad de _Virtual Machine Platform_:
  ```bash
  dism.exe /online /enable-feature /featurename:VirtualMachinePlatform /all /norestart
  ```
- Reiniciar el equipo
- Descargar la actualización del kernel de Linux
  - Mi recomendación es buscar es link en la [documentación oficial](https://docs.microsoft.com/en-us/windows/wsl/install-win10) para bajar el instalador más actualizado. El link utilizado al momento de redacción puede ser accedido [aquí](https://wslstorestorage.blob.core.windows.net/wslblob/wsl_update_x64.msi).
- Instalar WSL2 corriendo el ejecutable que acabamos de descargar
- Listo!

#### Hyper-V

De no poder o querer instalar WSL, podemos recurrir a Hyper-V. Hyper-V es una herramienta de virtualización de Windows 10 que debe ser habilitada. Esto puede hacerse manualmente desde Windows, o cuando instalemos Docker, ya que durante la instalación nos dejará tildar una casilla que habilita dicha función si está presente en nuestro dispositivo.

### Instalación Docker Desktop for windows

Hay una [guía en la página de Docker](https://docs.docker.com/docker-for-windows/install/) en donde esta detallado el proceso, pero a continuación les voy a describir los pasos para su conveniencia. De encontrarse con algún problema acudir a la documentación oficial puede ser un buen comienzo para solucionarlo.

Los siguientes pasos son para PCs que tengan Windows 10 profesional (Build 16299 y posteriores). De tener Windows 10 Home referirse al siguiente [link](https://docs.docker.com/docker-for-windows/install-windows-home/) y seguir las instrucciones.

- Descargar la versión estable de Docker Desktop de la página oficial -> [Docker Desktop for Windows](https://hub.docker.com/editions/community/docker-ce-desktop-windows/)
- Proceder con la instalación
  - Dejar tildadas las casillas de verificación. Hyper-V es opcional si se está utilizando WSL 2 como backend.
- Si no habiamos instalado WSL 2 antes de llegar hasta este paso nos preguntará si queremos hacerlo. Si ya está instalado creo que lo utiliza por defecto sin que tengamos que realizar ninguna otra nueva configuración.

### Verificación de la instalación

Para corroborar que todo este funcionando correctamente abriremos una ventana de línea de comandos en Windows (cmd) y corremos el siguiente comando:
```bash
docker run hello-world
```
Si todo funciona ok veremos el siguiente mensaje:

> Hello from Docker!
This message shows that your installation appears to be working correctly.
>
>To generate this message, Docker took the following steps:
 1. The Docker client contacted the Docker daemon.
 2. The Docker daemon pulled the "hello-world" image from the Docker Hub.
    (amd64)
 3. The Docker daemon created a new container from that image which runs the
    executable that produces the output you are currently reading.
 4. The Docker daemon streamed that output to the Docker client, which sent it
    to your terminal.

## Descargar imágenes necesarias para el entorno (Opcional)

***ghdl/ext*** -> Compilación y simulación de código VHDL empleando [GHDL](http://ghdl.free.fr/)

 > Ready-to-use images with GHDL and complements
(ghdl-language-server, GtkWave, VUnit, etc.)

```bash
docker pull ghdl/ext
```

***ghdl/synth*** -> Síntesis de código VHDL para configurar la placa EDU-FPGA

 > Ready-to-use images with GHDL and complements for synthesis:

 - tgingold/ghdlsynth-beta
 - cliffordwolf/icestorm
 - YosysHQ/nextpnr
 - YosysHQ/yosys
 - YosysHQ/SymbiYosys

```bash
docker pull ghdl/synth:beta
```
> NOTA: Esta imagen se encuentra en estado _beta_, pero ya fue probada con éxito en diseños sencillos. Cabe destacar que en Windows, a diferencia de en GNU/Linux, iceprog no se puede utilizar desde ghdl/synth:icestorm. Esto sucede ya que, tanto en Windows como en macOS, acceder a los puertos USB/COM del host desde el contenedor no pareciera estar soportado aún. **Es por eso que iceprog debe estar dispobible en el Host para poder bajar el diseño a la placa.**

***esolang/verilog*** -> Compilación y simulación de código Verilog empleando [icarusVerilog](http://iverilog.icarus.com/)

> Easily execute Verilog (Icarus Verilog) programming language

```bash
docker pull esolang/verilog:latest
```
***cranphin/icestorm*** -> Configuración de la placa EDU-FPGA con Verilog empleando la suite [iCEstorm](http://www.clifford.at/icestorm/)

> This is a very basic Docker file + Travis build configuration which
builds a IceStorm Docker image.

```bash
docker pull cranphin/icestorm:latest
```

## Zadig

La EDU-FPGA utiliza un chip FTDI para comunicarse con el ordenador. Si es la primera vez que conectamos un dispositivo FTDI a nuestra computadora, Windows buscará los drivers y los instalará automaticamente por defecto (si no fue deshabilitada esta opción manualmente con anterioridad). Sin embargo estos drivers no son compatibles con nuestro _toolchain_, por lo cual deberemos reemplazar uno de ellos.

Decimos "uno de ellos" ya que este chip posee dos canales de comunicación, por lo cual, luego de conectar la placa, veremos 4 nuevas entradas en nuestro Administrador de Dispositivos o _Device Manager_ (accesible haciendo click derecho sobre el icono de Windows en la barra de tareas y tildando la opcion correspondiente). Bajo puertos COM veremos dos nuevos puertos, y bajo dispositivos USB veremos dos nuevas entradas de Conversor Serie USB o USB Serial Converter (A y B).

La EDU-FPGA esta configurada de tal forma que el puerto A (ó 0 como veremos a continuación) esta conectado mediante SPI con la memoria flash presente en la placa. En la misma interfaz también se encuentra conectada la FPGA, la cual esta configurada para leer el archivo de configuración que descargaremos a la Flash y reconfigurarse cada vez que energicemos la placa. Es sobre este canal sobre el cual trabajaremos.

Pasos a seguir:

- [Descargar Zadig](https://zadig.akeo.ie/)
- Conectar la EDU-CIAA-FPGA
- Ejecutar el archivo que acabamos de descargar, el mismo no requiere instalación
- Hacer click en
  - Options -> List all devices
- En el menu desplegable seleccionar: _"Dual RS232-HS **(Interface 0)**"_
- Seleccionar libusbK como driver de reemplazo de la lista
- Click en _"Replace Driver"_
- De salir todo bien Zadig nos mostrará un mensaje de instalación correctamente
- **Desconectar y volver a conectar la placa**

>Nota: cada puerto USB es único y este proceso deberá ser repetido si conectamos la placa a otro puerto distinto. A pesar de esto el proceso debe ser realizado una única vez por puerto.

Si abrimos el Administrador de Dispositivos nuevamente podremos ver reflejados los cambios que acabamos de hacer.

# Instalacion GtkWave en Windows

- [Descargar GtkWave desde SourceForge](https://sourceforge.net/projects/gtkwave/)
- Descomprimir el archivo "gtkwave-x.x.xxx-bin-win32.zip" en un lugar seguro que no vayamos a ir cambiando de sitio frecuentemente
  - Para este ejemplo elegi guardarlo en el root del disco C:, lugar donde tengo instalado mi Windows.
- Para poder ejecutar gtkwave desde la consola de Windows sin tener que andar especificando el directorio donde lo tenemos instalado necesitaremos modificar la variable de entorno PATH de nuestro Windows.
  - Tipear en la barra de búsqueda "Configuración de Sistema Avanzada" ó "Advanced System Settings", dependiendo del idioma de nuestro Windows.
    - Alternativamente acceder desde el panel de control. Windows esta desfazando el panel de control así que no podemos asegurar por cuanto tiempo hacer esto seguirá siendo una opción válida.
    - En algunas versiones en cambio el menu se llama "Editar las variables de entorno del sistema"
  - Hacer click  en "Variables de Entorno" o "Environment Variables"
  - Click en "PATH" y luego en "Editar"
    - En esta nueva ventana podremos agregar el directorio donde se encuentra nuestro ejecutable de gtkwave. Para este ejemplo, como copiamos nuestra carpeta de GtkWave al root del disco C:, el directorio será:
  ```bash
  C:\gtkwave\bin
  ```
  - Click en "Nuevo"
    - Completar el campo con el directorio donde se encuentra gtkwave.exe
  - Abrir la consola (cmd.exe) y tipear "gtkwave" y apretar enter para comprobar el funcionamiento de la herramienta. Si todo está bien, el programa debería abrirse.


# Instalacion del toolchain IceStorm en Windows

Cómo mencionamos anteriormente IceProg no puede ser ejecutado desde un Docker en Windows, por este motivo deberemos utilizar una versión local del programa. Detallamos a continuación los pasos para su instalación.

  - [Descargar el toolchain IceStorm desde el github de FPGAwars](https://github.com/FPGAwars/toolchain-icestorm/releases/download/0.7/toolchain-icestorm-windows-7.zip)
  - Descomprimir el archivo "toolchain-icestorm-windows-7.zip" en un lugar seguro que no vayamos a ir cambiando de sitio frecuentemente. El toolchain es compatible con Windows 10.
    - Para este ejemplo elegi guardarlo en el root del disco C:, lugar donde tengo instalado mi Windows.
  - Para poder ejecutar iceprog desde la consola de Windows sin tener que andar especificando el directorio donde lo tenemos instalado necesitaremos modificar la variable de entorno PATH de nuestro Windows.
    - Tipear en la barra de búsqueda "Configuración de Sistema Avanzada" ó "Advanced System Settings", dependiendo del idioma de nuestro Windows.
      - Alternativamente acceder desde el panel de control. Windows esta desfazando el panel de control así que no podemos asegurar por cuanto tiempo hacer esto seguirá siendo una opción válida.
      - En algunas versiones en cambio el menu se llama "Editar las variables de entorno del sistema"
    - Hacer click  en "Variables de Entorno" o "Environment Variables"
    - Click en "PATH" y luego en "Editar"
      - En esta nueva ventana podremos agregar el directorio donde se encuentra nuestro ejecutable de iceprog. Para este ejemplo, como copiamos nuestra carpeta de toolchain-icestorm al root del disco C:, el directorio será:
    ```bash
    C:\toolchain-icestorm\bin
    ```
    - Click en "Nuevo"
      - Completar el campo con el directorio donde se encuentra iceprog.exe
    - Abrir la consola (cmd.exe) y tipear "iceprog" y apretar enter para comprobar el funcionamiento de la herramienta. Si todo está bien, deberíamos ver información sobre el uso de la herramienta en la consola.
       - Hay más herramientas que son parte del toolchain incluidas en el comprimido pero no las utilizaremos localmente ya que estas se encuentran integradas a nuestros entornos en Docker.
